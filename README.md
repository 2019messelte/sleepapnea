# SleepApnea
Challenge ENS pour le cours Algorithmes en DataScience:\
Detecting Sleep Apnea from raw physiological signals by Dreem.\
L'objectif de ce challenge est de construire un modèle pour détecter automatiquement les événements d'apnée du sommeil à partir de données PSG.

Nous proposons plusieurs methodes de prédictions différentes: SVM, FNN, CNN et ensemble methods. Les modèles, ainsi que la visualisation et le prétraitement des données, sont présentés dans des jupyter notebook.
\
Equipe:\
*Sacha Seksik: sacha.seksik@student-cs.fr\
*Emilie Messelt: emilie.messelt@student-cs.fr
## Organisation du git
Ce git est strucuté de manière suivante.
### Dossier Code
Contient les codes.
- metrics.py, fichier contenant la métrique, le calcul du score f1 sur les predictions (fourni)
- utils.py, fichier contenant les fonctions utilisées plusieurs fois
- visualisation.py, fichier permettant la visualisation des données (fourni)
- DataViz.ipynb, notebook de la visualisation et  des données
- SVM_Classe.ipynb, notebook du modèle SVM
- FNN_from_moments.ipynb, notebook du modèle FNN
- CNN_fast_Model.ipynb, notebook du modèle CNN avec ensemble method
- best_model_parameters, sauvegarde des paramètres du modèle CNN avec le meilleur résultat selon la métrique fournie
- best_results.npy, sauvegarde du meilleur résultat selon la métrique fournie
- svc_rbf_feature_model.sav, sauvegarde du modèle SVM avant la reduction du zerorate dans le training set
- svc_rbf_zerorate_model.sav, sauvegarde du meuilleur modèle de classification de la SVM (avec zerorate pour le training set)
- best_results_svm.npy, sauvegarde du meilleur résultat de la SVM selon la métrique fournie

### Dossier data:
Contient les données .csv, à completer avec les données .hp5y (trop lourd en mémoire pour mettre sur le git). 
Ces données sont accessible en s'inscirvant au challenge dreem de l'ENS [2], en téléchargant le dossier addtional_files_dreem.zip (intitulé supplementery files sur la page web). 

### Dossier benchmark:
Contient le résultats de prédiction du modèle benchmark (fourni).
## How to run 
1. Téléchargez le git: git clone https://gitlab-student.centralesupelec.fr/2019messelte/sleepapnea.git
2. Installez les paquets requis en utilisant pip3 et le fichier requirements.txt:
     sudo pip3 install -r requirements.txt
2. Téléchargez le complément des données (supplementery files sur la page web du challenge[2]). Dezipper et placer les fichiers suivants dans le dossier data: X_train.h5, X_test.h5. 
3. Les jupyter notebooks peuvent maintenant être lancés


## Sources de l'état de l'art:
[1] https://www.has-sante.fr/upload/docs/application/pdf/2012-06/place_et_conditions_de_realisation_de_la_polysomnographie_et_de_la_polygraphie_respiratoire_dans_les_troubles_du_sommeil_-_rapport_devaluation_2012-06-01_11-50-8_440.pdf \
[2] https://challengedata.ens.fr/participants/challenges/45/ 

## La présentation vidéo
La video de notre présentation se trouve sous le lien suivant: 
https://www.youtube.com/watch?v=d6fgPbDarQE



