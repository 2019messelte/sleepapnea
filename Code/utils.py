#%% File containing all utils functions for the notebooks to run 
# They appear in the following order: dataprocessing related functions, model training related functions. 
import numpy as np
import pandas as pd
import h5py 
import torch
import torch.nn as nn
import tqdm
import matplotlib.pyplot as plt



SIGNALS_NAME = ["AbdoBelt","AirFlow","PPG","ThorBelt","Snoring","SPO2","C4A1","O2A1"]

nb_features = len(SIGNALS_NAME)

#%%
def get_sample_data(file, index):
    """
    Returns data of a given file shaped in size (nb_features,9000)
    """
    return file['data'][index][2:].reshape(nb_features,9000)

def get_sample_target(mask, index):
    """
    Returns the target value of a given mask and index.
    - mask: function describing which samples correspond to which target
    - index: index (int)
    """
    return mask[index,1:]

def subsampling(data,rate=10):
    """
    Subsamples the given data with a sampling rate, per default 10.
    The sampling is done by taking the mean of each *rate* nbr of value of the initial data.
    - data: data to be sampled   
    - rate: sampling rate, by default 10 (int)     
    Returns:
    - data mean (np.array of size data/rate) or warning (string)
    """ 
    if data.shape == (nb_features,9000) and 9000%rate==0:
        return np.average(data.reshape(nb_features,9000//rate, rate), axis = 2)
    return 'Wrong dimension or rate'

def subsampling_var(data,rate=10):
    """
    Subsamples the given data with a specific samplingrate, per default 10.
    The sampling is done by taking the variance of each *rate* nbr of value of the initial data. 
    - data: data to be sampled   
    - rate: sampling rate, by default 10 (int) 
    Returns:
    - data variance (np.array of size data/rate) or warning (string)
    """ 
    if data.shape == (nb_features,9000) and 9000%rate==0:
        return np.var(data.reshape(nb_features,9000//rate, rate), axis = 2)
    return 'Wrong dimension or rate'

def subsampling_data(file, rate, special=0):
    """
    Subsamples the given data with a sampling rate, per default 10 and saves it in .npy format.
    The sampling is done by taking the mean of each *rate* nbr of value of the initial data using function subsampling.
    If the sampled data is already saved, the sampled data is loaded instead of generated.
    - data: data to be sampled   
    - rate: sampling rate, by default 10 (int)
    - special: 0 by default     
    Returns:
    - data mean (np.array of size data/rate) or warning (string)
    """ 
    traintest = ['training','test'][special]
    n_samples = len(file['data'])
    try: 
        
        sampled_data = np.load(traintest + '_dataset_subsampledx'+str(rate)+'.npy')
    except:
        sampled_data = np.zeros((n_samples,nb_features,9000//rate))
        for k in range(n_samples):
            if k%44==0:  print(k/44,'percent done')
            sampled_data[k]=subsampling(get_sample_data(file,k), rate=rate)
        np.save(traintest + '_dataset_subsampledx'+str(rate)+'.npy',sampled_data)
    return sampled_data

def subsampling_data_var(file, rate, special=0):
    """
    Subsamples the given data with a sampling rate, per default 10 and saves it in .npy format.
    The sampling is done by taking the mean of each *rate* nbr of value of the initial data using function subsampling_var.
    If the sampled data is already saved, the sampled data is loaded instead of generated.
    - data: data to be sampled   
    - rate: sampling rate, by default 10 (int) 
    - special: 0 by default     
    Returns:
    - data mean (np.array of size data/rate) or warning (string)
    """ 
    n_samples = len(file['data'])
    traintest = ['training','test'][special]
    try: 
        
        sampled_var = np.load(traintest + '_dataset_var_subsampledx'+str(rate)+'.npy')
    except:
        sampled_var = np.zeros((n_samples,nb_features,9000//rate))
        for k in range(n_samples):
            if k%44==0:  print(k/44,'percent done')
            sampled_var[k]=subsampling_var(get_sample_data(file,k), rate=rate)
        np.save(traintest + '_dataset_var_subsampledx'+str(rate)+'.npy',sampled_var)
    return sampled_var


def normalization(data):
    """
    Normalizes data between [-1, 1] using the mean and variance for each feature individually.
    - data: to be normalized, shaped according to feature 
    Returns:
    - data normalized
    """ 
    for k in range(nb_features):
        data[:,k] -= data[:,k].mean()
        data[:,k] *= 1/data[:,k].var()**0.5
    return data

def data_augmentation_per_subject(data, targets, new_seconds):
    """
    Performs data augmentation on a subjet (night) by generating new lenghts between samples sarting
    at a *new_second* starting point using given data. The targets are updated to correspond to of the augmented data.
    - data: data of the subject
    - targets: targets of the subjet
    - new_second: new starting point for data samples 
    Returns:
    - augmented data and augmented targets 
    """ 

    # here by length we mean length of data between two samples. the length of each samples is not going to change
    samples = len(data)
    old_length = data.shape[-1]
    targets_length = targets.shape[-1] 
    inputs_per_sec = old_length // targets_length 
    new_length = new_seconds * inputs_per_sec
    new_samples = old_length//new_length * samples - (old_length//new_length - 1)
    # print(new_samples,nb_features,old_length)
    augmented_data = np.zeros((new_samples, nb_features, old_length))
    augmented_targets = np.zeros((new_samples, targets_length))
    for k in range(nb_features):
        flattened_data=data[:,k].reshape(-1)
        for s in range(new_samples):
            augmented_data[s, k]= flattened_data[s*new_length: s*new_length+old_length] 
    flattened_targets = targets.reshape(-1)
    for s in range(new_samples):
        augmented_targets[s]=flattened_targets[s*new_seconds:s*new_seconds+targets_length]
    return augmented_data, augmented_targets

def data_augmentation(data, targets, new_seconds, n_subjects):
    """
    Performs data augmentation on a given number of subjects using the function data_aumentation_per_subject on
    given data, target and new_seconds given. The augmented data per subject to each other and the given data. 
    - data: data of the n_sujbects subjects 
    - targets: targets of the n_sujbects subjects 
    - new_second: new starting point for data samples 
    - n_sujects: number of subjects corresponding to the given data and targets
    Returns:
    - augmented data and augmented targets, include given data respectively given targets   
    """
    samples_sub = len(data)//n_subjects
    augmented_data, augmented_targets = data_augmentation_per_subject(data[:samples_sub], targets[:samples_sub], new_seconds)
    for s in range(1,n_subjects):
        augmented_data_new, augmented_targets_new = data_augmentation_per_subject(
                    data[samples_sub*s:samples_sub*(s+1)], targets[samples_sub*s:samples_sub*(s+1)], new_seconds)    
        augmented_data = np.concatenate((augmented_data, augmented_data_new))
        augmented_targets = np.concatenate((augmented_targets, augmented_targets_new))
    return augmented_data, augmented_targets

def eliminating_zeros(data, targets, kept_zeros_rate):
    """
    Reduces data and corresponding targets to keep only a given kept_zero_rate rate of targets = 0.
    - data: data corresponding to targets
    - target: targets 1 or 0
    - kept_zero_rate: rate of 0:s in the reduced target
    Returns:
    - reduced dataset and corresponding reduced targets
    """
    new_data, new_targets = [],[]
    for k in range(len(data)):
        if targets[k].mean()>0 or np.random.random()<kept_zeros_rate:
            new_data.append(data[k])
            new_targets.append(targets[k])
    return np.array(new_data), np.array(new_targets)

def reshape_1s(data, targets):
    """
    Reshapes given data and targets to the shape of a suject. 
    Returns: 
    - data_1s of shape (nbr of targets of a subject, nbr of features of a subject, nbr of points for target)
    - targets_1s of shape (nbr of a subject)
    """
    targets_1s =targets.reshape(-1)
    data_1s = data.reshape(len(targets_1s),nb_features,-1)
    return data_1s, targets_1s



def low_band_filter(targets, positive_only=1):
    """
    Filters the targets to only positive
    """
    # target shaped by subject is better
    lb_targets = targets.copy()
    for s in range(len(targets)):
        for i in range(1, len(targets[0])-1):
            if targets[s,i-1]==targets[s,i+1]:
                if not(positive_only) or targets[s,i+1]:
                    lb_targets[s,i] = targets[s,i+1]
    return lb_targets
                

# %%
class DatasetTransformer(torch.utils.data.Dataset):
    def __init__(self, inputs_dataset, targets_dataset):
        self.inputs = inputs_dataset
        self.targets = targets_dataset

    def __getitem__(self, index):
        inpt, target = self.inputs[index], self.targets[index]
        return inpt, target

    def __len__(self):
        return len(self.inputs)


#%% Loads data in the right format for DL
def dataloader(train_data, train_targets, test_data, test_targets, batch_size):
    train_dataset = DatasetTransformer(train_data, train_targets)
    valid_dataset = DatasetTransformer(test_data, test_targets)
    #Dataloader
    num_threads = 0   # Loading the dataset is using Y CPU threads
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                            batch_size=batch_size,
                                            shuffle=True,              # <-- this reshuffles the data at every epoch
                                            num_workers=num_threads)
    valid_loader = torch.utils.data.DataLoader(dataset=valid_dataset,
                                            batch_size=batch_size,
                                            shuffle=False,
                                            num_workers=num_threads)
    print("The train set contains {} samples, in {} batches".format(len(train_loader.dataset), len(train_loader)))
    print("The validation set contains {} samples, in {} batches".format(len(valid_loader.dataset), len(valid_loader)))
    return train_loader, valid_loader

#%% Train model and returns model (for DL)
def train_model(train_loader, valid_loader, model, criterion, optimizer, scheduler, num_epochs, special_weight = None):
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
                dataloaders = train_loader
            else:
                model.eval()   # Set model to evaluate mode
                dataloaders = valid_loader
            
            dataset_sizes = len(dataloaders.dataset)

            running_loss = 0.0
            running_corrects = 0


            # Iterate over data.
            for inputs, labels in tqdm.tqdm(dataloaders):
                
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    preds = (outputs > 0.5)
                    if criterion == 'special':
                        loss = nn.functional.binary_cross_entropy(outputs, labels.float(), (torch.ones_like (labels.float()) / special_weight  +  (1.0 - 1.0 / special_weight) * labels.float()) )
                    else:
                        loss = criterion(outputs, labels.float())                


                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            if phase == 'train':
                try:
                    scheduler.step(metrics= 1)
                except:
                    scheduler.step()
                
            epoch_loss = running_loss / dataset_sizes
            epoch_acc = running_corrects.double() / dataset_sizes

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = model.state_dict()

        print()

    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model

####################################################


def base_freq(y, int, fe, SIGNALS_NAME):
    """Plots the module of the TFD and phase of the TFD given signal and calculates its fundamental frequency.
    - y: signal
    - int: integer which indicates what signal is beeing processed
    - fe: frequency rate of the signal
    - SIGNALS_NAME: names of the possible signals
    Returns: 
    -freq_in_hertz, fundemntal frequency of the signal in Hz 
    """
    N = len(y)
    print("Nombre d'échantillons : ", len(y))
    print("Fréquence d'échantillonnage : ", fe)
    S = np.fft.fft(y)
    te = np.arange(0,N)/fe
    plt.figure(1)
    freq = np.fft.fftfreq(N)*fe
    plt.plot(np.fft.fftshift(freq),np.fft.fftshift(np.abs(y).real/fe), label=SIGNALS_NAME[int])
    plt.title('Module de la T.F.D.')
    plt.legend()
    plt.xlabel('Fréquence (Hz)')
    plt.ylabel('Amplitude (u.a.)')
    plt.figure(3)
    plt.plot(np.fft.fftshift(freq),np.fft.fftshift(np.angle(S).real), label=SIGNALS_NAME[int])
    plt.title('Phase de la T.F.D.')
    plt.legend()
    plt.grid(True)
    plt.xlabel('Fréquence (Hz)')
    plt.ylabel('Phase (rd)')
    plt.show()      
    w = np.fft.fft(y)
    freqs = np.fft.fftfreq(len(w))
    print(freqs.min(), freqs.max())
    # Find the peak in the coefficients
    idx = np.argmax(np.abs(w))
    freq = freqs[idx]
    freq_in_hertz = abs(freq * fe)
    return freq_in_hertz


def get_mean_variance(data,SIGNALS_NAME,win=np.arange(4400),normalization=0):
    """
    Creates dataframe with mean and variance of each feature.
    - data given
    - SIGNALS_NAME: names of each feature
    - win, window
    - normalization, if data is normalized or not
    Returns
    - df_mean: dataframe contaning the means of each feature
    - df_variance: dataframe containg the variance of each feature
    
    """
    df_mean = pd.DataFrame(columns = SIGNALS_NAME)
    df_variance = pd.DataFrame(columns= SIGNALS_NAME)
    if normalization==1: 
        normalization(data)
    for w in range(len(win)):
        means = []
        variances = []
        for i in range(nb_features):  
            mean = np.mean(data[win[w]][i]) 
            means.append(mean)
            variance = np.var(data[win[w]][i])
            variances.append(variance)
        df_mean.loc[w] = means
        df_variance.loc[w] = variances
    return df_mean, df_variance

def separate_data(data, mask, norm=0):
    """
    Splits data given in two different datasets, one for targets = 1 and one for target = 0.
    - data to be splited, conaining all data 
    - mask: function describing which samples correspond to which target
    - norm, if data should be normalized or not (O=not, by default)
    Returns: 
    - apneas, dataset containing all data for targets=1
    - no_apneas, dataset containing all data for targets=0
    """
    apnea = []
    no_apnea = []
    if norm==1: 
        normalization(data)
    (w,s) = mask[:,1:].shape
    re = 0
    for window in range(w):
        for second in range(s):
            if mask[window,second] == 1: 
                apnea.append(data[window,:,second])
                re+=1
            else: 
                no_apnea.append(data[window,:,second])
    apneas = np.array(apnea).reshape(re, 8)
    no_apneas = np.array(no_apnea).reshape((w*s)-re, 8)
    return apneas, no_apneas



if __name__=='main':
    data_x_train = h5py.File('../data/X_train.h5')
    data_x_test = h5py.File('../data/X_test.h5')
    mask_x_train = np.array(pd.read_csv('../data/X_train.csv'))
    mask_x_test = np.array(pd.read_csv('../data/X_test.csv'))
    mask_y_train = np.array(pd.read_csv('../data/y_train.csv'))

    print('main')


# %%
